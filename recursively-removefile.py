#!/usr/bin/python
#author: Robert Chen (robertchen117@gmail.com)
#date: Oct. 8, 2016
#test1
#test2
#test3

#test4
#test5

import os
import argparse
#need python > 3.5


def recursive_scandir(path):
    for filename in os.scandir(path):
        if filename.is_file():
            yield filename.path
        if filename.is_dir():
            yield from recursive_scandir(filename.path)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', help='path to be processed', required=True)
    args = parser.parse_args()

    path = args.path

    if path:
        for file in recursive_scandir(path):
            #print(file)

            ext = os.path.splitext(file)[-1].lower()
            if ext == '.nfo':
                print('removing ' + file)
                os.remove(file)
            shortname = os.path.splitext(os.path.basename(file))[0]
            #if basename.upper() == 'RARBG':
            #print(shortname)
            if shortname.upper().startswith('RARBG'):
                print('removing ' + file)
                os.remove(file)
            if shortname.upper().startswith('WWW'):
                print('removing ' + file)
                os.remove(file)

